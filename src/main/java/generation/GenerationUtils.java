package generation;

import tjp.wiji_object.drawing.GameMap;
import tjp.wiji_object.drawing.ObjectGeometry;
import tjp.wiji_object.drawing.PrecedenceClass;
import tjp.wiji_object.location.Coordinates;
import tjp.wiji_object.location.ImmutableMultimapLocator;
import tjp.wiji_object.location.Locator;
import tjp.wiji_object.objects.GameObject;
import tjp.wiji.representations.Graphic;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji_object.generation.Builder;

import java.awt.*;

import static drawing.Palette.EVEN_LESS_DARK_GRAY;
import static drawing.Palette.LESS_DARK_GRAY;

public class GenerationUtils {
    private static final ObjectGeometry WALL_GEOMETRY = new ObjectGeometry(Color.DARK_GRAY,
            Color.BLACK, Graphic.FILLED_CELL);
    private static final ObjectGeometry FLOOR_GEOMETRY = new ObjectGeometry(LESS_DARK_GRAY,
            Color.BLACK, Graphic.FILLED_CELL);
    private static final ObjectGeometry DOOR_GEOMETRY = new ObjectGeometry(EVEN_LESS_DARK_GRAY,
            Color.BLACK, Graphic.FILLED_CELL);

    private static final GameObject.Factory WALL_FACTORY = new GameObject.Factory("Wall",
            WALL_GEOMETRY, true, false, PrecedenceClass.DEFAULT);
    private static final GameObject.Factory FLOOR_FACTORY = new GameObject.Factory("Floor",
            FLOOR_GEOMETRY, false, false, PrecedenceClass.FLOOR);
    private static final GameObject.Factory DOOR_FACTORY = new GameObject.Factory("Door",
            DOOR_GEOMETRY, false, false,PrecedenceClass.DEFAULT);

    private static final Builder BUILDER = ImmutableMultimapLocator.BUILDER;
    private static final Locator ROOM = BUILDER.buildRectangle(
            WALL_FACTORY, FLOOR_FACTORY, 4, 3);

    private static final Locator VERTICAL_ROOM = BUILDER.buildRectangle(
            WALL_FACTORY, FLOOR_FACTORY, 3, 4);

    public static GameObject makeMainChar() {
        return new GameObject(".", new ObjectGeometry(
                Color.WHITE,null,Graphic.AT_SYMBOL
        ), false, false, PrecedenceClass.DEFAULT);
    }

    public static GameMap makeGameMap() {
        Locator horizontal = BUILDER.buildRectangle(
                WALL_FACTORY, FLOOR_FACTORY, 22, 4)
                .translate(31,8);

        Locator vertical = BUILDER.buildRectangle(
                WALL_FACTORY, FLOOR_FACTORY, 4, 22)
                .translate(40,0);

        Locator mainHallways = vertical.symmetricDifference(horizontal);

        Locator mainMap = mainHallways
                .mergingUnion(ROOM.copy().translate(37,0))
                .mergingUnion(ROOM.copy().translate(37,2))
                .mergingUnion(ROOM.copy().translate(37,4))
                .mergingUnion(ROOM.copy().translate(37,6))

                .mergingUnion(ROOM.copy().translate(43,0))
                .mergingUnion(ROOM.copy().translate(43,2))
                .mergingUnion(ROOM.copy().translate(43,4))
                .mergingUnion(ROOM.copy().translate(43,6))

                .mergingUnion(ROOM.copy().translate(43,11))
                .mergingUnion(ROOM.copy().translate(43,13))
                .mergingUnion(ROOM.copy().translate(43,15))
                .mergingUnion(ROOM.copy().translate(43,17))
                .mergingUnion(ROOM.copy().translate(43,19))

                .mergingUnion(ROOM.copy().translate(37,11))
                .mergingUnion(ROOM.copy().translate(37,13))
                .mergingUnion(ROOM.copy().translate(37,15))
                .mergingUnion(ROOM.copy().translate(37,17))
                .mergingUnion(ROOM.copy().translate(37,19))

                .mergingUnion(VERTICAL_ROOM.copy().translate(31,11))
                .mergingUnion(VERTICAL_ROOM.copy().translate(33,11))
                .mergingUnion(VERTICAL_ROOM.copy().translate(35,11))

                .mergingUnion(VERTICAL_ROOM.copy().translate(46,11))
                .mergingUnion(VERTICAL_ROOM.copy().translate(48,11))
                .mergingUnion(VERTICAL_ROOM.copy().translate(50,11))

                .mergingUnion(VERTICAL_ROOM.copy().translate(31,5))
                .mergingUnion(VERTICAL_ROOM.copy().translate(33,5))
                .mergingUnion(VERTICAL_ROOM.copy().translate(35,5))

                .mergingUnion(VERTICAL_ROOM.copy().translate(46,5))
                .mergingUnion(VERTICAL_ROOM.copy().translate(48,5))
                .mergingUnion(VERTICAL_ROOM.copy().translate(50,5))
                ;

        return new GameMap(mainMap, 78, 23,
                ImageRepresentation.ALL_BLACK);
    }

    public static void addChar(final GameMap map, final GameObject mainChar) {
        map.add(mainChar, new Coordinates(42, 6));
    }
}
