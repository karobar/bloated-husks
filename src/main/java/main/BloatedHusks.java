package main;

import tjp.wiji_object.drawing.GameMap;
import generation.GenerationUtils;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.Subject;

import tjp.wiji_object.objects.GameObject;
import screens.StepToMapScreenController;
import screens.TitleScreen;
import tjp.wiji.drawing.ApplicationFactory;
import tjp.wiji.drawing.bootstrap.AppBootstrapper;
import tjp.wiji.gui.screen.Screen;
import tjp.wiji.gui.screen.ScreenChangeController;
import tjp.wiji.gui.screen.ScreenContext;

import java.net.URISyntaxException;

public class BloatedHusks {
    private static ScreenContext makeScreenContext() {
        final Subject<Boolean> backStream = BehaviorSubject.create();
        final Subject<Screen> forwardStream = BehaviorSubject.create();

        final ScreenChangeController controller = new ScreenChangeController() {
            @Override
            public void stepScreenBackwards() {
                backStream.onNext(true);
            }

            @Override
            public void stepScreenForwards(Screen screen) {
                forwardStream.onNext(screen);
            }
        };

        final GameMap map = GenerationUtils.makeGameMap();
        final GameObject mainObject = GenerationUtils.makeMainChar();
        GenerationUtils.addChar(map, mainObject);

        final StepToMapScreenController stepToMapScreenController = new StepToMapScreenController(
                controller,
                map,
                mainObject
                );

        final Screen sampleScreen = new TitleScreen(controller,
                stepToMapScreenController);
        final ScreenContext screenContext = ScreenContext.create(sampleScreen);

        backStream.subscribe((ignored) -> screenContext.stepScreenBackwards());
        forwardStream.subscribe(screenContext::stepScreenForwards);

        return screenContext;
    }

    public static void main(final String[] args) throws URISyntaxException {
        ApplicationFactory.runApp("BLOATED HUSKS", makeScreenContext(),
                AppBootstrapper.create().getFilesModule());
    }
}
