package screens;

import com.google.common.collect.ImmutableSet;
import tjp.wiji.event.GameEvent;
import tjp.wiji.gui.gui_element.GuiElement;
import tjp.wiji.gui.screen.Screen;
import tjp.wiji.gui.screen.ScreenChangeController;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji_object.drawing.MapContext;
import tjp.wiji_object.location.Coordinates;

import java.util.Collection;

public class InspectScreen extends Screen {
    private final MapContext mapContext;
    protected InspectScreen(ScreenChangeController screenChangeController,
                            final MapContext map, final Coordinates startingPosition) {
        super( screenChangeController);
        this.mapContext = map;
    }

    /**
     * @deprecated
     */
    @Override
    protected Collection<GuiElement> getGuiElements() {
        return ImmutableSet.of();
    }

    @Override
    protected ImageRepresentation getCurrentCell(int x, int y) {
        return mapContext.get(x, y).orElse(ImageRepresentation.ALL_BLACK);
    }

    @Override
    protected void handleFrameChange() {

    }

    @Override
    public void stepToScreenTrigger() {
        System.out.println("Entering Inspect Screen");
    }

    @Override
    public void handleEvent(GameEvent gameEvent) {

    }
}
