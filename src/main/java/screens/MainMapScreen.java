package screens;

import com.badlogic.gdx.Input;
import com.google.common.collect.ImmutableSet;
import tjp.wiji_object.drawing.GameMap;
import tjp.wiji_object.drawing.MapContext;
import tjp.wiji_object.exceptions.ObjectNotFoundException;
import tjp.wiji_object.location.Coordinates;
import tjp.wiji_object.objects.GameObject;
import tjp.wiji.event.GameEvent;
import tjp.wiji.gui.gui_element.GuiElement;
import tjp.wiji.gui.screen.Screen;
import tjp.wiji.gui.screen.ScreenChangeController;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji_object.viewing.StaticCamera;

import java.util.Collection;

import static com.google.common.base.Preconditions.checkNotNull;

public class MainMapScreen extends Screen {
    private final MapContext mapContext;
    private final GameObject mainObject;

    public MainMapScreen(final ScreenChangeController screenChangeController,
                         final GameMap gameMap,
                         final GameObject mainObject) {
        super(screenChangeController);
        this.mapContext = new MapContext(this.getCellExtents(), gameMap,
                new StaticCamera(gameMap, new Coordinates(-1, -1)));
        this.mainObject = checkNotNull(mainObject);
    }

    @Override
    protected Collection<GuiElement> getGuiElements() {
        return ImmutableSet.of();
    }

    @Override
    protected ImageRepresentation getCurrentCell(final int x, final int y) {
        return mapContext.get(x, y).orElse(ImageRepresentation.ALL_BLACK);
    }

    @Override
    protected void handleFrameChange() { }

    @Override
    public void stepToScreenTrigger() { }

    @Override
    public void handleEvent(GameEvent gameEvent) {
        if (gameEvent.getIntCode() == Input.Keys.X) {
            try {
                screenChangeController.stepScreenForwards(new InspectScreen(
                        screenChangeController,
                        mapContext, mapContext.locate(mainObject)));
            } catch (ObjectNotFoundException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
