package screens;

import tjp.wiji_object.drawing.GameMap;
import tjp.wiji_object.objects.GameObject;
import tjp.wiji.gui.screen.ScreenChangeController;

public class StepToMapScreenController {
    private final ScreenChangeController screenChangeController;
    private final GameMap gameMap;
    private final GameObject mainObject;

    public StepToMapScreenController(final ScreenChangeController screenChangeController,
                                     final GameMap gameMap,
                                     final GameObject mainObject) {
        this.screenChangeController = screenChangeController;
        this.gameMap = gameMap;
        this.mainObject = mainObject;
    }

    public void stepToMapScreen() {
        screenChangeController.stepScreenForwards(new MainMapScreen(
                screenChangeController,
                gameMap,
                mainObject));
    }
}
