package screens;

import com.badlogic.gdx.Input;
import com.google.common.collect.ImmutableSet;
import tjp.wiji.event.GameEvent;
import tjp.wiji.gui.gui_element.GuiElement;
import tjp.wiji.gui.gui_element.NewGuiText;
import tjp.wiji.gui.gui_element.placers.CoordPlacer;
import tjp.wiji.gui.gui_element.placers.Placer;
import tjp.wiji.gui.screen.Screen;
import tjp.wiji.gui.screen.ScreenChangeController;
import tjp.wiji.representations.ImageRepresentation;

import java.awt.*;
import java.util.Collection;

public class TitleScreen extends Screen {
    private final StepToMapScreenController stepToMapScreenController;
    public TitleScreen(final ScreenChangeController screenChangeController,
                       final StepToMapScreenController stepToMapScreenController) {
        super(screenChangeController);
        this.stepToMapScreenController = stepToMapScreenController;
    }

    /**
     * @deprecated
     */
    @Override
    protected Collection<GuiElement> getGuiElements() {
        return ImmutableSet.of();
    }

    @Override
    protected ImmutableSet<Placer> getGuiReps() {
        return ImmutableSet.of(
                new CoordPlacer(new NewGuiText("B L O A T E D", Color.RED), 29, 11),
                new CoordPlacer(new NewGuiText("H U S K S", Color.RED), 42, 12),
                new CoordPlacer(new NewGuiText("ENTER", Color.DARK_GRAY), 38, 18)
        );
    }

    @Override
    protected ImageRepresentation getCurrentCell(int i, int i1) {
        return ImageRepresentation.ALL_BLACK;
    }

    @Override
    protected void handleFrameChange() { }

    @Override
    public void stepToScreenTrigger() { }

    @Override
    public void handleEvent(final GameEvent gameEvent) {
        if (gameEvent.getIntCode() == Input.Keys.ENTER) {
            stepToMapScreenController.stepToMapScreen();
        }
    }
}
